# -*- coding: utf-8 -*-
from datetime import datetime
from django.db import models
from query_set import CarQuerySet,CompanyQuerySet,PriceListQuerySet
# Create your models here.



class Car(models.Model):
    car_name = models.CharField(max_length=200)
    car_price = models.FloatField(default=0)
    time_stamp = models.DateTimeField(default=datetime.now())
    companys = models.ManyToManyField("Company", through='PriceList')
    objects =CarQuerySet.as_manager()

    def __unicode__(self):
        return self.car_name




class Company(models.Model):

    Citys_choices = (( "TLV","Tel-aviv")
                     , ("HIF","Hifa" )
                     , ("AHD","Ashdod", )
                     , ("JRS","Jerusalem" ))

    BOOL_CHOICES = ((True, 'Yes'), (False, 'No'))
    company_name = models.CharField(max_length=200,primary_key=True)
    company_city = models.CharField(max_length=10,choices=Citys_choices)
    company_phone_number = models.CharField(max_length=10)
    is_active = models.BooleanField(choices=BOOL_CHOICES)
    objects = CompanyQuerySet.as_manager()


    def __unicode__(self):
        return self.company_name


class PriceList(models.Model):
    company = models.ForeignKey("Company")
    car = models.ForeignKey("Car")
    price_at_company = models.FloatField(default=0)
    objects = PriceListQuerySet.as_manager()


    def __unicode__(self):
        return u"Company: {0} Car: {1} Car price: {2}".format(self.company.__unicode__(),self.car.__unicode__(),self.price_at_company)

