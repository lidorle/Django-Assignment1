/**
 * Created by DEV on 17/05/2016.
 */

function hide_tables() {
    $(document).ready(function () {
        $("table").hide();
    });
}


function hide_text_box() {
    $(document).ready(function () {
       $(".text_boxs").hide();
    });
}
hide_tables();
hide_text_box();

$(document).ready(function() {


    $("#show1").click(function(){
        hide_tables();
        hide_text_box();
        $("#lattar_text_box").show();

    });
    $("#show2").click(function(){
        hide_tables();
        hide_text_box();
        $("#compsny_list_text_box").show();

    });
    $("#show3").click(function(){
        hide_text_box();
        hide_tables();
        $("#price_list_for_car_text_box").show();

    });



});

$(document).ready(function() {
    $("#show5").click(function (e) {
        hide_text_box();
        hide_tables();
        $('.rows').remove();
        $("#cars_table").show();
        e.preventDefault();

        $.ajax({
            url: 'http://127.0.0.1:8000/polls/cars_from_active_company/',
            dataType:  'json',
            type: 'POST',
            processData:false,
            contentType:false,
            cache: false,
            async: false,
            success: function (data){
                $.each(data, function () {
                    console.log(this.car_name);
                    $('#cars_table').last().append("<tr class='rows'><td>" + this.car_name + "</td><td>"+'$'+this.car_price + "</td><td>"+this.time_stamp + "</td></tr>");
                });
            },
        });

    });
});


$(document).ready(function() {
    $("#submit1").click(function (e) {
        hide_text_box();
        hide_tables();
        $('.rows').remove();
        $("#companys_table").show();
        e.preventDefault();

        $.ajax({
            url: 'http://127.0.0.1:8000/polls/companys_name_start_with/'+ ($('#text_filed1').val())+'/?type=Ajax',
            dataType:  'json',
            type: 'GET',
            processData:false,
            contentType:false,
            cache: false,
            async: false,
            success: function (data){
                $.each(data, function () {
                    $('#companys_table').last().append("<tr class='rows'><td>" + this['company_name'] + "</td><td>"+this['company_city'] + "</td><td>"+this['company_phone_number']+"</td> <td>"+this['is_active']+"</td></tr>");
                });
            },
            error:function(data){
                hide_tables();
                alert("There is no sach companys!");
            },
        });
        $("#text_filed1").val('');

    });
});


$(document).ready(function() {
    $("#submit2").click(function (e) {
        hide_text_box();
        hide_tables();
        $("#price_list_by_company_table").show();
        $('.rows').remove();
        e.preventDefault();

        $.ajax({
            url: 'http://127.0.0.1:8000/polls/PriceList_by_company/'+ ($('#text_filed2').val())+'/?type=Ajax',
            dataType:  'json',
            type: 'GET',
            processData:false,
            contentType:false,
            cache: false,
            async: false,
            success: function (data){
                $.each(data, function () {
                    console.log(this);
                    $('#price_list_by_company_table').last().append("<tr class='rows'><td>" + this['car__car_name'] + "</td><td>"+'$'+this['car__car_price'] + "</td></tr>");
                });
            },
            error:function(data){
                hide_tables();
                alert("There is no sach companys!");
            },
        });
        $("#text_filed2").val('');

    });
});


$(document).ready(function() {
    $("#submit3").click(function (e) {
        hide_text_box();
        hide_tables();
        e.preventDefault();
        $('.rows').remove();
        $("#price_list_for_car_table").show();
        $.ajax({
            url: 'http://127.0.0.1:8000/polls/price_list_for_car/'+ ($('#text_filed3').val())+'/?type=Ajax',
            dataType:  'json',
            type: 'GWT',
            processData:false,
            contentType:false,
            cache: false,
            async: false,
            success: function (data){
                $.each(data, function () {
                    $('#price_list_for_car_table').last().append("<tr class='rows'><td>" + this['car__car_name'] + "</td><td>"+this['company__company_name'] + "</td><td>"+this['price_at_company'] + "</td></tr>");
                });
            },
            error:function(data){
                hide_tables();
                alert("There is no sach a car in the data base!");
            },
        });
        $("#text_filed3").val('');

    });
});

$(document).ready(function() {
    $("#show4").click(function (e) {
        hide_text_box();
        hide_tables();
        $('.rows').remove();
        $("#price_list_by_minmun_price_table").show();
        e.preventDefault();

        $.ajax({
            url: 'http://127.0.0.1:8000/polls/price_list_by_minmun_price/?type=Ajax',
            dataType:  'json',
            type: 'GET',
            processData:false,
            contentType:false,
            cache: false,
            async: false,
            success: function (data){
                $.each(data, function () {
                    console.log(this);
                    $('#price_list_by_minmun_price_table').last().append("<tr class='rows'><td>" + this['car'] + "</td><td>"+this['company'] + "</td><td>"+'$'+this['min_price'] + "</td></tr>");
                });
            },
        });

    });
});
