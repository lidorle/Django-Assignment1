# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import pandas as pd
from django.shortcuts import render
from .models import Car,Company,PriceList
from django.http import Http404
import json
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.views.decorators.csrf import csrf_exempt
import urlparse
from django.http import HttpResponse

# Create your views here.



def make_excel_file(name,obj):
    df = pd.DataFrame(obj)
    df.to_excel(name,engine='openpyxl',index=False)
    return name


@csrf_exempt
def cars_from_active_company(request):
    obj = list(Car.objects.cars_company_is_active())
    if not obj:
        raise Http404("The company: There are no company that i avtive.")
    print obj
    if request.is_ajax():
        return HttpResponse(json.dumps(obj, cls=DjangoJSONEncoder), content_type='application/json')

    file_name = "cars_from_active_company.xlsx"
    file = open(make_excel_file(file_name,obj),'rb')
    response = HttpResponse(file,content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename= '+ file_name
    return response

@csrf_exempt
def companys_name_start_with(request,letter):

    obj = list(Company.objects.companys_name_start_with(letter))
    if not obj:
        raise Http404("No sech company that start the latter:.".format(letter))

    if request.GET.get('type')=='Ajax':
        return HttpResponse(json.dumps(obj), content_type='application/json')


    file_name = make_excel_file("price_list_for_car_{0}.xlsx".format(letter),obj)
    file = open(make_excel_file(file_name,obj),'rb')
    response = HttpResponse(file,content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename= '+ file_name
    return response

@csrf_exempt
def PriceList_by_company(request,company):

    obj = list(PriceList.objects.sort_by_company(company))
    if not obj:
        raise Http404("The company:{0} may not in the data base or there are no cars.".format(company))


    if request.GET.get('type')=='Ajax':
        return HttpResponse(json.dumps(obj), content_type='application/json')
    file_name = "PriceList_by_company_{0}.xlsx".format(company)
    file = open(make_excel_file(file_name,obj),'rb')
    response = HttpResponse(file,content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename= '+ file_name
    return response


@csrf_exempt
def price_list_for_car(request,car):

    obj = list(PriceList.objects.car_price_in_all_companys(car))
    print obj
    if not obj:
        raise Http404("The car:{0} may not be in the data base or there there no relations to companys.".format(car))
    if request.GET.get('type')=='Ajax':
        return HttpResponse(json.dumps(obj), content_type='application/json')

    file_name = "price_list_for_car_{0}.xlsx".format(car)
    file = open(make_excel_file(file_name,obj),'rb')
    response = HttpResponse(file,content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename= '+ file_name
    return response

@csrf_exempt
def price_list_by_minmun_price(request):
    obj = list(PriceList.objects.comany_minmum_price())
    if not obj:
        raise Http404("The car:{0} may not be in the data base or there there no relations to companys.".format())

    if request.GET.get('type')=='Ajax':
        # return HttpResponse(serializers.serialize('json',obj,use_natural_foreign_keys=True ), content_type='application/json')
        return HttpResponse(json.dumps(obj), content_type='application/json')

    file_name = "price_list_by_minmun_price.xlsx"
    file = open(make_excel_file(file_name,obj),'rb')
    response = HttpResponse(file,content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename= '+ file_name
    return response



def home(request):
    return render(request, 'home.html')

