# -*- coding: utf-8 -*-

from rest_framework import serializers
from polls.models import Company

class CompanySerialzier(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('company_name','company_city','company_phone_number','is_active')
