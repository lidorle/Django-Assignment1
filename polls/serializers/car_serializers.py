# -*- coding: utf-8 -*-

from rest_framework import serializers
from polls.models import Car

class CarSerialzier(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = ('car_name','car_price','time_stamp','companys')
