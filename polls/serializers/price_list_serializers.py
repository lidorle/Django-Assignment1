# -*- coding: utf-8 -*-

from rest_framework import serializers
from polls.models import PriceList

class PriceListSerialzier(serializers.ModelSerializer):
    class Meta:
        model = PriceList
        fields = ('company','car','price_at_company')
