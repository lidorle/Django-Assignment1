# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Min
from django.db.models import F



class CarQuerySet(models.QuerySet):
    def cars_company_is_active(self):
        return self.filter(companys__is_active='True').distinct().values()


class CompanyQuerySet(models.QuerySet):
    def companys_name_start_with(self,letter):
        return self.filter(is_active='True').filter(company_name__startswith = letter).values()


class PriceListQuerySet(models.QuerySet):
    def sort_by_company(self,company):
        return self.filter(company__company_name=company).values("car__car_name","car__car_price")

    def car_price_in_all_companys(self,car):
        return self.filter(car__car_name = car).values("company__company_name","price_at_company","car__car_name")

    def comany_minmum_price(self):
        q = self.annotate(min_price=Min('company__pricelist__price_at_company'))\
            .filter(price_at_company=F('min_price')).values('car','company','min_price')
        return  q