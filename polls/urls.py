# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.utils.http import urlquote_plus
from . import views
from polls.viewsets.car_viewset import CarViewSet
from polls.viewsets.company_viewset import CompanyViewSet
from polls.viewsets.price_list_viewset import PriceListViewSet
from django.conf.urls import patterns, include
from rest_framework_extensions.routers import ExtendedDefaultRouter
from django.contrib import admin


app_name = 'polls'

admin.autodiscover()

router = ExtendedDefaultRouter()
{
     router.register('car', CarViewSet, base_name='car'),
     router.register('company',CompanyViewSet,base_name='company'),
     router.register('price_list',PriceListViewSet,base_name='price_list'),
     router.register('price_list', PriceListViewSet, base_name='price_list')
       .register('car', CarViewSet, base_name='car', parents_query_lookups=['car'])
}

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^cars_from_active_company/$', views.cars_from_active_company, name='cars_from_active_company'),
    url(r'^companys_name_start_with/(?P<letter>.+)/$', views.companys_name_start_with, name='companys_name_start_with'),
    url(r'^price_list_for_car/(?P<car>.+)/$', views.price_list_for_car, name='price_list_for_car'),
    url(r'^PriceList_by_company/(?P<company>.+)/$', views.PriceList_by_company, name='PriceList_by_company'),
    url(r'^price_list_by_minmun_price/$', views.price_list_by_minmun_price, name='price_list_by_minmun_price'),

]