# -*- coding: utf-8 -*-
from django.test import TestCase
from polls.models import Car,Company,PriceList
from django.utils import timezone
from django.utils.encoding import force_text



class test_views_response_json(TestCase):
    def setUp(self):
        self.company=Company(company_name="ellit",company_city="Ashdod",company_phone_number='05458797654',is_active='yes')
        self.company.save()
        self.car=Car(car_price=45345,car_name="reno",time_stamp=timezone.now())
        self.car.save()
        self.price_list=PriceList(company=self.company,car=self.car,price_at_company=4312)
        self.price_list.save()


    def test_json_price_list_for_car(self):
        response = self.client.get('http://127.0.0.1:8000/polls/price_list_for_car/reno/?type=Ajax')
        print  force_text(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_text(response.content), [{"price_at_company": 4312.0, "company__company_name": "ellit", "car__car_name": "reno"}])

    def test_json_companys_name_start_with(self):
        response = self.client.get('http://127.0.0.1:8000/polls/companys_name_start_with/ellit/?type=Ajax')
        print  force_text(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_text(response.content), [{u'company_phone_number': u'05458797654', u'company_city': u'Ashdod', u'is_active': True, u'company_name': u'ellit'}])


    def test_json_price_list_for_car(self):
        response = self.client.get('http://127.0.0.1:8000/polls/PriceList_by_company/ellit/?type=Ajax')
        print  force_text(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_text(response.content), [{"car__car_name": "reno", "car__car_price": 45345.0}]
)


    def test_json_price_list_for_car(self):
        response = self.client.get('http://127.0.0.1:8000/polls/price_list_by_minmun_price/?type=Ajax')
        print  force_text(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_text(response.content),[{"car": 1, "company": "ellit", "min_price": 4312.0}])


