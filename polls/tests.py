# -*- coding: utf-8 -*-
from django.test import TestCase
from polls.models import Car,Company,PriceList
from django.utils import timezone
import pandas as pd



# Create your tests here.



class test_cars_from_active_company(TestCase):
    def setUp(self):
        self.company=Company(company_name="ellit",company_city="Ashdod",company_phone_number='05458797654',is_active='yes')
        self.company.save()
        self.car=Car(car_price=45345,car_name="reno",time_stamp=timezone.now())
        self.car.save()
        self.price_list=PriceList(company=self.company,car=self.car,price_at_company=4312)
        self.price_list.save()

    def test_data_frame(self):
         obj1 = list(Car.objects.all().filter(companys__is_active='True').filter(companys__company_name=u'אסם').values())
         df1 = pd.DataFrame(obj1)
         df1.to_excel("lidor1.xls",engine='openpyxl',index=False)
         df2 = pd.read_excel('http://127.0.0.1:8000/polls/cars_from_active_company/')
         self.assertItemsEqual(df1,df2)

    def test_cars_from_active_company_query_not_empay(self):
        self.assertGreater(0,len(self.obj))

    def test_reqest(self):
        response = self.client.get('http://127.0.0.1:8000/polls/cars_from_active_company/')
        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)


class test_companys_name_start_with(TestCase):
    def setUp(self):
        self.company=Company(company_name="ellit",company_city="Ashdod",company_phone_number='05458797654',is_active='yes')
        self.company.save()
        self.car=Car(car_price=45345,car_name="reno",time_stamp=timezone.now())
        self.car.save()
        self.price_list=PriceList(company=self.company,car=self.car,price_at_company=4312)
        self.price_list.save()

    def test_data_frame(self):
         obj1 = list(Company.objects.all().filter(is_active='True').filter(company_name__startswith = 'e').values())
         df1 = pd.DataFrame(obj1)
         df1.to_excel("lidor1.xls",engine='openpyxl',index=False)
         df2 = pd.read_excel('http://127.0.0.1:8000/polls/companys_name_start_with/e/')
         self.assertItemsEqual(df1,df2,check_names=True)

    def test_cars_from_active_company_query_not_empay(self):
        self.assertGreater(0,len(self.obj))

    def test_reqest(self):
        response = self.client.get('http://127.0.0.1:8000/polls/companys_name_start_with/e/')
        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)


class test_price_list_for_car(TestCase):
    def setUp(self):
        self.company=Company(company_name="ellit",company_city="Ashdod",company_phone_number='05458797654',is_active='yes')
        self.company.save()
        self.car=Car(car_price=45345,car_name="reno",time_stamp=timezone.now())
        self.car.save()
        self.price_list=PriceList(company=self.company,car=self.car,price_at_company=4312)
        self.price_list.save()

    def test_data_frame(self):
         obj1 = list(PriceList.objects.all().filter(car__car_name = "reno").values("company__company_name","price_at_company","car__car_name"))
         df1 = pd.DataFrame(obj1)
         df1.to_excel("lidor1.xls",engine='openpyxl',index=False)
         df2 = pd.read_excel('http://127.0.0.1:8000/polls/price_list_for_car/reno/')
         self.assertItemsEqual(df1,df2)

    def test_cars_from_active_company_query_not_empay(self):
        self.assertGreater(0,len(self.obj))

    def test_reqest(self):
        response = self.client.get('http://127.0.0.1:8000/polls/price_list_for_car/reno/')
        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)


class test_PriceList_by_company(TestCase):
    def setUp(self):
        self.company=Company(company_name="ellit",company_city="Ashdod",company_phone_number='05458797654',is_active='yes')
        self.company.save()
        self.car=Car(car_price=45345,car_name="reno",time_stamp=timezone.now())
        self.car.save()
        self.price_list=PriceList(company=self.company,car=self.car,price_at_company=4312)
        self.price_list.save()

    def test_data_frame(self):
         obj1 = list(PriceList.objects.all().filter(car__car_name = "reno").values("company__company_name","price_at_company","car__car_name"))
         df1 = pd.DataFrame(obj1)
         df1.to_excel("lidor1.xls",engine='openpyxl',index=False)
         df2 = pd.read_excel('http://127.0.0.1:8000/polls/price_list_for_car/reno/')
         self.assertFrameEqual(df1,df2)

    # def test_cars_from_active_company_query_not_empay(self):
    #     self.assertGreater(0,len(self.obj))

    def test_reqest(self):
        response = self.client.get('http://127.0.0.1:8000/polls/PriceList_by_company/ellit/')
        # Check that the response is 200 OK.
        print response.content
        self.assertEqual(response.status_code, 200)