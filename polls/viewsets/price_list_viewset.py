# -*- coding: utf-8 -*-

from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from polls.models import PriceList
from polls.serializers.price_list_serializers import PriceListSerialzier

class PriceListViewSet(NestedViewSetMixin, ModelViewSet):
      serializer_class = PriceListSerialzier
      queryset = PriceList.objects.all()