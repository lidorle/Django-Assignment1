# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Car,Company,PriceList
# Register your models here.

class PriceListAdmin(admin.ModelAdmin):
    def car__name(self, obj):
         return obj.car.car_name

    list_display = ('price_at_company','car__name','company')

class CarListAdmin(admin.ModelAdmin):
    list_display = ('car_name','car_price','time_stamp')

admin.site.register(Car,CarListAdmin)
admin.site.register(Company)
admin.site.register(PriceList,PriceListAdmin)